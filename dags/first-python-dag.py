from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.python import PythonOperator
from airflow.providers.http.sensors.http import HttpSensor
import requests

def call_api():
    response = requests.get('https://dog.ceo/api/breeds/image/random')
    if response.status_code == 200:
        print(response.json())
    else:
        print('Request failed with status code:', response.status_code)


default_args = {
    "owner": "airflow",
    "email_on_failure": False,
    "email_on_retry": False,
    "email": "admin@localhost.com",
    "retries": 1,
    "retry_delay": timedelta(minutes=5) #will need to import timedelta from datetime
}

with DAG('first-python-dag', 
         start_date=datetime(2023, 8, 23), 
         schedule_interval='@daily',
         default_args=default_args,
         catchup=False) as dag:
    # making call to the api via function
    making_api_call = PythonOperator(
        task_id='make_api_call',
        python_callable = call_api
    )

    check_http_response = HttpSensor(
        task_id='check_http_response' ,
        http_conn_id='http_dog_api_conn',
        endpoint='api/breeds/image/random',
        response_check=lambda response:"success" in response.text,
        poke_interval=5,
        timeout=20
    )
