from datetime import datetime

try:
    from airflow.operators.empty import EmptyOperator
except ModuleNotFoundError:
    from airflow.operators.dummy import DummyOperator as EmptyOperator  # type: ignore

from airflow import DAG
from airflow.utils.edgemodifier import Label
from airflow.providers.dbt.cloud.hooks.dbt import DbtCloudHook
from airflow.providers.dbt.cloud.operators.dbt import (DbtCloudRunJobOperator,
                                                       DbtCloudGetJobRunArtifactOperator,
                                                       DbtCloudListJobsOperator
                                                       )
from airflow.providers.dbt.cloud.sensors.dbt import DbtCloudJobRunSensor

DBT_ACCOUNT_ID = 191512
DBT_CONN_ID = 'ss-airflow-dbt-conn'

with DAG(
    dag_id='first_dbt_cloud_dag',
    start_date=datetime(2023, 8, 23),
    schedule='@once',
    catchup=False,
    default_args={"dbt_cloud_conn_id":DBT_CONN_ID, "account_id":DBT_ACCOUNT_ID}
) as dag:
    
    begin = EmptyOperator(task_id="begin")
    end = EmptyOperator(task_id="end")

    trigger_job_run1 = DbtCloudRunJobOperator(
        task_id="trigger_job_run1",
        job_id=405192, # change this
        check_interval=10,
        timeout=300
    )

    get_run_results_artifact = DbtCloudGetJobRunArtifactOperator(
        task_id="get_run_results_artifact", 
        run_id=trigger_job_run1.output, 
        path="run_results.json"
    )

    job_run_sensor = DbtCloudJobRunSensor(
        task_id="job_run_sensor", 
        run_id=trigger_job_run1.output, 
        timeout=20
    )

    list_dbt_jobs = DbtCloudListJobsOperator(
        task_id="list_dbt_jobs", 
        project_id=280869) # change this
    
    begin >> Label("Wait with sensor") >> trigger_job_run1
    [get_run_results_artifact, job_run_sensor, list_dbt_jobs] >> end