from datetime import datetime
from airflow import DAG
try:
    from airflow.operators.empty import EmptyOperator
except ModuleNotFoundError:
    from airflow.operators.dummy import DummyOperator as EmptyOperator  # type: ignore
from airflow.providers.snowflake.operators.snowflake import SnowflakeOperator

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2023, 8, 28),
    'depends_on_past': False,
    'retries': 1,
}

dag = DAG(
    'dbt_project_dag',
    default_args=default_args,
    schedule_interval='@once',
    catchup=False,
)

# Define a DummyOperator to simulate downloading the CSV (replace with actual logic)
download_csv_task = EmptyOperator(
    task_id='download_csv',
    dag=dag,
)

# Define the Snowflake Operator to create the table and copy data from local file
upload_to_snowflake_task = SnowflakeOperator(
    task_id='upload_to_snowflake',
    sql="""
    USE ROLE ACCOUNTADMIN;
    USE DATABASE RAW;
    USE WAREHOUSE DBT_WH;
    
    CREATE OR REPLACE TABLE RAW.SUSTAINABLE_ENERGY.GLOBAL_DATA (
        ENTITY VARCHAR,
        YEAR NUMBER(38, 0),
        "access_to_electricity [%_of_population]" FLOAT,
        ACCESS_TO_CLEAN_FUELS_FOR_COOKING FLOAT,
        RENEWABLE_ENERGY_GENERATING_CAPACITY_PER_CAPITA FLOAT,
        "financial_flows_to_developing_countries [US_$]" NUMBER(38, 0),
        "renewable_energy_share_in_the_total_final_energy_consumption [%]" FLOAT,
        "electricity_from_fossil_fuels [TWh]" FLOAT,
        "electricity_from_nuclear [TWh]" FLOAT,
        "electricity_from_renewables [TWh]" FLOAT,
        "low-carbon_electricity [%_electricity]" FLOAT,
        "primary_energy_consumption_per_capita [kWh/person]" FLOAT,
        "energy_intensity_level_of_primary_energy [MJ/$2017_PPP_GDP]" FLOAT,
        VALUE_CO2_EMISSIONS_KT_BY_COUNTRY NUMBER(38, 0),
        "renewables [%_equivalent_primary_energy]" FLOAT,
        GDP_GROWTH FLOAT,
        GDP_PER_CAPITA FLOAT,
        "density [p/km2]" NUMBER(38, 0), -- Corrected column name
        "land_area [km2]" NUMBER(38, 0),
        LATITUDE FLOAT,
        LONGITUDE FLOAT
    );
    
    COPY INTO RAW.SUSTAINABLE_ENERGY.GLOBAL_DATA (
        ENTITY,
        YEAR,
        "access_to_electricity [%_of_population]",
        ACCESS_TO_CLEAN_FUELS_FOR_COOKING,
        RENEWABLE_ENERGY_GENERATING_CAPACITY_PER_CAPITA,
        "financial_flows_to_developing_countries [US_$]",
        "renewable_energy_share_in_the_total_final_energy_consumption [%]",
        "electricity_from_fossil_fuels [TWh]",
        "electricity_from_nuclear [TWh]",
        "electricity_from_renewables [TWh]",
        "low-carbon_electricity [%_electricity]",
        "primary_energy_consumption_per_capita [kWh/person]",
        "energy_intensity_level_of_primary_energy [MJ/$2017_PPP_GDP]",
        VALUE_CO2_EMISSIONS_KT_BY_COUNTRY,
        "renewables [%_equivalent_primary_energy]",
        GDP_GROWTH,
        GDP_PER_CAPITA,
        "density [p/km2]", -- Corrected column name
        "land_area [km2]",
        LATITUDE,
        LONGITUDE
    )

    FROM '@RAW.SUSTAINABLE_ENERGY.GLOBAL_DATA_STAGE/'
    FILE_FORMAT = (
        TYPE = 'CSV'
        FIELD_DELIMITER = ','
        SKIP_HEADER = 1
    );
    """,
    snowflake_conn_id='ss-airflow-dbt-conn',
    autocommit=True,
    dag=dag,
)

# Define task dependencies
download_csv_task >> upload_to_snowflake_task


