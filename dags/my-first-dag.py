from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.bash import BashOperator
from textwrap import dedent

default_args = {
    "owner": "airflow",
    "email_on_failure": False,
    "email_on_retry": False,
    "email": "admin@localhost.com",
    "retries": 1,
    "retry_delay": timedelta(minutes=5) # will need to import timedelta from datetime
}

with DAG(dag_id='my-first-dag', 
         start_date=datetime(2023, 8, 23), 
         schedule_interval='@daily', 
         default_args=default_args, 
         catchup=False) as dag:
    
    t1 = BashOperator(
    task_id="print_date",
    bash_command="date",
    )
    
    t2 = BashOperator(
    task_id="sleep",
    depends_on_past=False,
    bash_command="sleep 5",
    retries=3,
    )

    templated_command = dedent(
    """
    {% for i in range(5) %}
    echo "{{ ds }}"
    echo "{{ macros.ds_add(ds, 7)}}"
    {% endfor %}
    """
    )
    
    t3 = BashOperator(
    task_id="templated",
    depends_on_past=False,
    bash_command=templated_command,
    )

    t1 >> (t2,t3)
